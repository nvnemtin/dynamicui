package jam.nxngrp.dynamicuiprototype.domain.command

import jam.nxngrp.dynamicuiprototype.domain.item.Widget

sealed class WidgetCommand {
    abstract val widget: Widget

    sealed class Red : WidgetCommand() {
        class UseOwnLogic(
            override val widget: Widget
        ) : Red()
    }

    sealed class Common : WidgetCommand() {
        data class Navigate(
            override val widget: Widget,
            val navId: String // whatever
        ) : Common()

        class SelfUpdate(
            override val widget: Widget
        ) : Common()

        class ExecuteRunnable(
            override val widget: Widget,
            val runnable: Runnable
        ) : Common()

        class ProduceBlocking(
            override val widget: Widget
        ) : Common()

        class StartProducer(
            override val widget: Widget
        ) : Common()
    }
}
