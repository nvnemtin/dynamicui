package jam.nxngrp.dynamicuiprototype.domain

import io.reactivex.Single
import jam.nxngrp.dynamicuiprototype.domain.item.Widget

interface WidgetDataSource {
    fun getScreen(widgetAmount: Int): Single<List<Widget>>
    fun getWidgetData(widget: Widget): Single<Widget>
}