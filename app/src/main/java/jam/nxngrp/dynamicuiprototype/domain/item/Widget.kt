package jam.nxngrp.dynamicuiprototype.domain.item

import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommandDispatcher

sealed class Widget {
    abstract val id: String
    abstract val content: String
    abstract val hasButton: Boolean
    abstract val needRefreshing: Boolean
    abstract var dispatcher: WidgetCommandDispatcher? // in real app we'd want it also as val

    abstract fun copyWithNeedsRefreshing(): Widget

    data class Red(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Orange(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Yellow(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Green(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Cyan(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Blue(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }

    data class Purple(
        override val id: String,
        override val content: String,
        override val hasButton: Boolean,
        override val needRefreshing: Boolean,
        override var dispatcher: WidgetCommandDispatcher? = null
    ) : Widget() {
        override fun copyWithNeedsRefreshing() = this.copy(needRefreshing = true)
    }
}