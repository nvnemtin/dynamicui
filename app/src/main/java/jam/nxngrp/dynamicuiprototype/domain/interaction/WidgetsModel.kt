package jam.nxngrp.dynamicuiprototype.domain.interaction

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import jam.nxngrp.dynamicuiprototype.data.DefaultWidgetDataSource
import jam.nxngrp.dynamicuiprototype.domain.WidgetDataSource
import jam.nxngrp.dynamicuiprototype.domain.command.DefaultWidgetCommandDispatcher
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommandDispatcher
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import kotlin.random.Random

class WidgetsModel : WidgetsInteractor {
    private val dataSource: WidgetDataSource = DefaultWidgetDataSource()
    private val screen: Subject<List<Widget>> = BehaviorSubject.createDefault<List<Widget>>(emptyList()).toSerialized()
    private val navSignal: Subject<String> = PublishSubject.create<String>().toSerialized()
    private val dispatcher: WidgetCommandDispatcher =
        DefaultWidgetCommandDispatcher(
            widgetsInteractor = this,
            navigationListener = {
                navSignal.onNext(it)
            }
        )
    private val compositeDisposable = CompositeDisposable()

    override fun getScreen(): Observable<List<Widget>> {
        screen.onNext(emptyList())
        dispatcher.clearAll()
        compositeDisposable.add(
            dataSource
                .getScreen(Random.nextInt(from = 10, until = 21))
                .subscribeOn(Schedulers.io())
                .subscribeBy(onSuccess = { list ->
                    list.forEach { it.attachDispatcher(dispatcher) }
                    screen.onNext(list)
                    list.forEach {
                        reloadWidget(it)
                    }
                }, onError = Throwable::printStackTrace)
        )
        return screen
    }

    override fun unsubscribe() {
        dispatcher.clearAll()
        compositeDisposable.clear()
    }

    override fun reloadWidget(widget: Widget, forced: Boolean) {
        if (widget.needRefreshing || forced) {
            if (forced) {
                updateWidget(widget.copyWithNeedsRefreshing())
            }
            compositeDisposable.add(
                dataSource
                    .getWidgetData(widget)
                    .subscribeOn(Schedulers.io())
                    .subscribeBy(onSuccess = this::updateWidget, onError = Throwable::printStackTrace)
            )
        }
    }

    override fun navigationFlow(): Observable<String> = navSignal

    override fun updateWidget(widget: Widget) {
        widget.attachDispatcher(dispatcher)
        val current = screen.blockingFirst().toMutableList()
        val index = current.indexOfFirst {
            it.id == widget.id
        }
        if (index >= 0) {
            current[index] = widget
        } else {
            current.add(widget) // wtf
        }
        screen.onNext(current.toList())
    }
}


private fun Widget.attachDispatcher(dispatcher: WidgetCommandDispatcher) { // will check later if WeakReference here will make sense
    this.dispatcher = dispatcher
}