package jam.nxngrp.dynamicuiprototype.domain.command

import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import jam.nxngrp.dynamicuiprototype.domain.WidgetHandler
import jam.nxngrp.dynamicuiprototype.domain.WidgetStateBlockingProducer
import jam.nxngrp.dynamicuiprototype.domain.WidgetStateProducer
import jam.nxngrp.dynamicuiprototype.domain.interaction.WidgetsInteractor
import jam.nxngrp.dynamicuiprototype.presentation.handler.RedWidgetHandler
import java.util.concurrent.ConcurrentHashMap

interface WidgetCommandDispatcher {
    fun dispatchCommand(command: WidgetCommand)
    fun putHandler(id: String, widgetHandler: WidgetHandler)
    fun clearAll()
}

class DefaultWidgetCommandDispatcher(
    private val widgetsInteractor: WidgetsInteractor,
    private var navigationListener: (String) -> Unit
) : WidgetCommandDispatcher {
    private var widgetHandlers = ConcurrentHashMap<String, WidgetHandler>() // no need in concurrent for now
    private var widgetProducerDisposables = ConcurrentHashMap<String, Disposable>()

    override fun dispatchCommand(command: WidgetCommand) {
        when (command) { // this can be delegated to custom dispatchers, for better adhering to SRP
            is WidgetCommand.Red -> {
                when (command) {
                    is WidgetCommand.Red.UseOwnLogic -> {
                        val handler: WidgetHandler?
                        synchronized(widgetHandlers) {
                            handler = widgetHandlers[command.widget.id]
                        }
                        if (handler is RedWidgetHandler) {
                            handler.ownLogic(command.widget)
                        }
                    }
                }
            }
            is WidgetCommand.Common -> {
                when (command) {
                    is WidgetCommand.Common.Navigate -> navigationListener.invoke(command.navId)
                    is WidgetCommand.Common.SelfUpdate -> widgetsInteractor.reloadWidget(command.widget, forced = true)
                    is WidgetCommand.Common.ExecuteRunnable -> command.runnable.run()
                    is WidgetCommand.Common.ProduceBlocking -> {
                        val handler: WidgetHandler?
                        synchronized(widgetHandlers) {
                            handler = widgetHandlers[command.widget.id]
                        }
                        if (handler is WidgetStateBlockingProducer) {
                            val newWidgetState = handler.produce(command.widget)
                            widgetsInteractor.updateWidget(newWidgetState)
                        }
                    }
                    is WidgetCommand.Common.StartProducer -> {
                        val handler: WidgetHandler?
                        synchronized(widgetHandlers) {
                            handler = widgetHandlers[command.widget.id]
                        }
                        if (handler is WidgetStateProducer) {
                            synchronized(widgetProducerDisposables) {
                                widgetProducerDisposables[command.widget.id]?.let {
                                    if (!it.isDisposed) {
                                        it.dispose()
                                    }
                                }
                            }
                            val widgetProducerDisposable = handler
                                .produce(command.widget)
                                .subscribeOn(Schedulers.io())
                                .subscribeBy(onNext = {
                                    widgetsInteractor.updateWidget(it)
                                }, onError = Throwable::printStackTrace)
                            synchronized(widgetProducerDisposables) {
                                widgetProducerDisposables[command.widget.id] = widgetProducerDisposable
                            }
                        }
                    }
                }
            }
        }
    }

    override fun putHandler(id: String, widgetHandler: WidgetHandler) {
        synchronized(widgetHandlers) {
            widgetHandlers.put(id, widgetHandler)
        }
    }

    override fun clearAll() {
        synchronized(widgetHandlers) {
            widgetHandlers.clear()
        }
        synchronized(widgetProducerDisposables) {
            widgetProducerDisposables.values.forEach {
                if (!it.isDisposed) {
                    it.dispose()
                }
            }
        }
    }
}