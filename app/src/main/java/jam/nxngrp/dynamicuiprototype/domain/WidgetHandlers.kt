package jam.nxngrp.dynamicuiprototype.domain

import io.reactivex.Observable
import jam.nxngrp.dynamicuiprototype.domain.item.Widget

interface WidgetHandler

interface WidgetStateBlockingProducer: WidgetHandler {
    fun produce(widget: Widget): Widget
}

interface WidgetStateProducer: WidgetHandler {
    fun produce(widget: Widget): Observable<Widget>
}
