package jam.nxngrp.dynamicuiprototype.domain.interaction

import io.reactivex.Observable
import jam.nxngrp.dynamicuiprototype.domain.item.Widget

interface WidgetsInteractor {
    fun getScreen(): Observable<List<Widget>>
    fun reloadWidget(widget: Widget, forced: Boolean = false)
    fun updateWidget(widget: Widget)
    fun navigationFlow(): Observable<String>
    fun unsubscribe()
}