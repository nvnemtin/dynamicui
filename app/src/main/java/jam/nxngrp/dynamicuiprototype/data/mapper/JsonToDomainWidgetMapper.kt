package jam.nxngrp.dynamicuiprototype.data.mapper

import jam.nxngrp.dynamicuiprototype.data.web.WidgetJsonDataDto
import jam.nxngrp.dynamicuiprototype.data.web.WidgetJsonDto
import jam.nxngrp.dynamicuiprototype.data.web.WidgetType
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import java.util.*

class JsonToDomainWidgetMapper {
    fun map(json: WidgetJsonDto, id: String? = null): Widget {
        return when (json.type) {
            WidgetType.RED -> Widget.Red(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.ORANGE -> Widget.Orange(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.YELLOW -> Widget.Yellow(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.GREEN -> Widget.Green(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.CYAN -> Widget.Cyan(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.BLUE -> Widget.Blue(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
            WidgetType.PURPLE -> Widget.Purple(
                id = id ?: UUID.randomUUID().toString(),
                content = json.data?.content ?: "",
                hasButton = json.data?.hasButton ?: false,
                needRefreshing = checkNeedsRefreshing(json.data)
            )
        }
    }

    private fun checkNeedsRefreshing(data: WidgetJsonDataDto?): Boolean {
        if (data == null) {
            return true
        }
        return data.content.isNullOrBlank() || data.hasButton == null
    }
}