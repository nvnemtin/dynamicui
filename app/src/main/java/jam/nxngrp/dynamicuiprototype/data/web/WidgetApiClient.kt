package jam.nxngrp.dynamicuiprototype.data.web

import android.util.Log
import io.reactivex.Single
import kotlin.random.Random

interface WidgetApiClient {
    fun getScreen(amount: Int): Single<List<WidgetJsonDto>>

    fun loadWidgetData(type: WidgetType): Single<WidgetJsonDataDto>
}

class DummyWidgetApiClient : WidgetApiClient {
    private val widgetFactory = DummyWidgetFactory()

    override fun getScreen(amount: Int): Single<List<WidgetJsonDto>> {
        return Single.fromCallable {
            val result = mutableListOf<WidgetJsonDto>()
            repeat(amount) {
                result.add(widgetFactory.get(getRandomWidgetType()))
            }
            return@fromCallable result.toList()
        }
    }

    override fun loadWidgetData(type: WidgetType): Single<WidgetJsonDataDto> {
        return Single.fromCallable {
            Log.d("ZZZ", "load widget data for $type request was fired")
            try {
                Thread.sleep(Random.nextLong(1000, 5000))
            } catch (e: InterruptedException) {

            }
            Log.d("ZZZ", "load widget data for $type request was completed")
            return@fromCallable widgetFactory.getData(type)
        }
    }
}

class DummyWidgetFactory {
    fun get(type: WidgetType): WidgetJsonDto {
        return when (type) {
            WidgetType.RED -> WidgetJsonDto(type, WidgetJsonDataDto("red widget content", true))
            WidgetType.ORANGE -> WidgetJsonDto(type, WidgetJsonDataDto("orange widget content", true))
            WidgetType.YELLOW -> WidgetJsonDto(type, WidgetJsonDataDto(null, true))
            WidgetType.GREEN -> WidgetJsonDto(type, WidgetJsonDataDto(null, null))
            WidgetType.CYAN -> WidgetJsonDto(type, null)
            WidgetType.BLUE -> WidgetJsonDto(type, WidgetJsonDataDto("blue widget content", null))
            WidgetType.PURPLE -> WidgetJsonDto(type, WidgetJsonDataDto("", true))
        }
    }

    fun getData(type: WidgetType): WidgetJsonDataDto {
        return  WidgetJsonDataDto(content = "updated ${type.name} data", hasButton = true)
    }
}