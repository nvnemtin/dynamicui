package jam.nxngrp.dynamicuiprototype.data

import io.reactivex.Single
import jam.nxngrp.dynamicuiprototype.data.mapper.JsonToDomainWidgetMapper
import jam.nxngrp.dynamicuiprototype.data.web.DummyWidgetApiClient
import jam.nxngrp.dynamicuiprototype.data.web.WidgetApiClient
import jam.nxngrp.dynamicuiprototype.data.web.WidgetJsonDto
import jam.nxngrp.dynamicuiprototype.data.web.toEnum
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.domain.WidgetDataSource

class DefaultWidgetDataSource : WidgetDataSource {
    private val apiClient: WidgetApiClient = DummyWidgetApiClient()
    private val mapper = JsonToDomainWidgetMapper()

    override fun getScreen(widgetAmount: Int): Single<List<Widget>> {
        return apiClient
            .getScreen(widgetAmount)
            .map { list ->
                list.map {
                    mapper.map(it)
                }
            }
    }

    override fun getWidgetData(widget: Widget): Single<Widget> {
        return apiClient
            .loadWidgetData(widget.toEnum())
            .map { data ->
                WidgetJsonDto(type = widget.toEnum(), data = data)
            }
            .map {
                mapper.map(it, widget.id)
            }
    }
}