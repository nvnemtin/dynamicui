package jam.nxngrp.dynamicuiprototype.data.web

import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import kotlin.random.Random

class WidgetJsonDto(
    val type: WidgetType,
    val data: WidgetJsonDataDto?
)

class WidgetJsonDataDto(
    val content: String?,
    val hasButton: Boolean?
)

enum class WidgetType {
    RED, ORANGE, YELLOW, GREEN, CYAN, BLUE, PURPLE
}

fun getRandomWidgetType(): WidgetType {
    val rnd = Random.nextInt(until = WidgetType.values().size)
    return WidgetType.values()[rnd]
}

fun Widget.toEnum(): WidgetType {
    return when (this) {
        is Widget.Red -> WidgetType.RED
        is Widget.Orange -> WidgetType.ORANGE
        is Widget.Yellow -> WidgetType.YELLOW
        is Widget.Green -> WidgetType.GREEN
        is Widget.Cyan -> WidgetType.CYAN
        is Widget.Blue -> WidgetType.BLUE
        is Widget.Purple -> WidgetType.PURPLE
    }
}