package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.YellowWidgetViewHolder

class YellowWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Yellow, Widget, YellowWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): YellowWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_yellow, parent, false)
        return YellowWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Yellow
    }

    override fun onBindViewHolder(item: Widget.Yellow, holder: YellowWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}