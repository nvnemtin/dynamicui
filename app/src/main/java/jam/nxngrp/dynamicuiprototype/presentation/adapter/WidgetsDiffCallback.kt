package jam.nxngrp.dynamicuiprototype.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import jam.nxngrp.dynamicuiprototype.domain.item.Widget

class WidgetsDiffCallback(
    private val oldItems: List<Widget>,
    private val newItems: List<Widget>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}