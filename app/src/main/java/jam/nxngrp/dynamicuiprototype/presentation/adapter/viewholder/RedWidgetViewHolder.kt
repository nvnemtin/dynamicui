package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.handler.RedWidgetHandler
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_red.*

class RedWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Widget.Red? = null

    init {
        widget_btn_own_method.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Red.UseOwnLogic(it)
                it.dispatcher?.putHandler(it.id, RedWidgetHandler(containerView.context, "SomeString"))
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Red) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) { View.VISIBLE } else { View.GONE }
        widget_btn_own_method.visibility = if (widget.hasButton) { View.VISIBLE } else { View.GONE }
    }
}