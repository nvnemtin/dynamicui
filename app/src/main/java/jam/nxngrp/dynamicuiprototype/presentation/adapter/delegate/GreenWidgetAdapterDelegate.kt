package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.GreenWidgetViewHolder

class GreenWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Green, Widget, GreenWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): GreenWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_green, parent, false)
        return GreenWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Green
    }

    override fun onBindViewHolder(item: Widget.Green, holder: GreenWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}