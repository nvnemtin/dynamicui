package jam.nxngrp.dynamicuiprototype.presentation.handler

import jam.nxngrp.dynamicuiprototype.domain.WidgetStateBlockingProducer
import jam.nxngrp.dynamicuiprototype.domain.item.Widget

class CyanWidgetBlockingProducer: WidgetStateBlockingProducer {
    override fun produce(widget: Widget): Widget {
        return if (widget is Widget.Cyan) {
            Widget.Cyan(
                id = widget.id,
                content = widget.content + " .",
                hasButton = widget.hasButton,
                needRefreshing = false,
                dispatcher = widget.dispatcher
            )
        } else {
            widget
        }
    }
}