package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.BlueWidgetViewHolder

class BlueWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Blue, Widget, BlueWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): BlueWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_blue, parent, false)
        return BlueWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Blue
    }

    override fun onBindViewHolder(item: Widget.Blue, holder: BlueWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}