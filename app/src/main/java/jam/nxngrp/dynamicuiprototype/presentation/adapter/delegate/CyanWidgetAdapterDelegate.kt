package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.CyanWidgetViewHolder

class CyanWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Cyan, Widget, CyanWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): CyanWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_cyan, parent, false)
        return CyanWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Cyan
    }

    override fun onBindViewHolder(item: Widget.Cyan, holder: CyanWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}