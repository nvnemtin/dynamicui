package jam.nxngrp.dynamicuiprototype.presentation.secondary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import jam.nxngrp.dynamicuiprototype.domain.interaction.WidgetsInteractor
import jam.nxngrp.dynamicuiprototype.domain.interaction.WidgetsModel
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.util.SingleLiveEvent

class SecondViewModel: ViewModel() {
    private val interactor: WidgetsInteractor = WidgetsModel()
    private var disposable: Disposable? = null
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState.Loading }
    private val _navSignal = SingleLiveEvent<String>()
    private val compositeDisposable = CompositeDisposable()
    val viewState = _viewState
    val navSignal: LiveData<String> = _navSignal

    init {
        compositeDisposable.add(
            interactor
                .navigationFlow()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    _navSignal.value = it
                }, onError = Throwable::printStackTrace)
        )
    }

    fun refresh() {
        disposeCurrent()
        disposable = interactor
            .getScreen()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                if (it.isEmpty()) {
                    _viewState.value = ViewState.Loading
                } else {
                    _viewState.value = ViewState.Content(it)
                }
            }, onError = {
                _viewState.value = ViewState.Error(it.localizedMessage)
            })
    }

    override fun onCleared() {
        interactor.unsubscribe()
        disposeCurrent()
        super.onCleared()
    }

    sealed class ViewState {
        object Loading : ViewState()

        data class Content(
            val widgets: List<Widget>
        ) : ViewState()

        data class Error(
            val msg: String
        ) : ViewState()
    }

    private fun disposeCurrent() {
        disposable?.let {
            if (!it.isDisposed) {
                it.dispose()
            }
        }
    }
}
