package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.OrangeWidgetViewHolder

class OrangeWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Orange, Widget, OrangeWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): OrangeWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_orange, parent, false)
        return OrangeWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Orange
    }

    override fun onBindViewHolder(item: Widget.Orange, holder: OrangeWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}