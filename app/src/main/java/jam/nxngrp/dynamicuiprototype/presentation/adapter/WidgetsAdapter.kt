package jam.nxngrp.dynamicuiprototype.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate.*

class WidgetsAdapter : ListDelegationAdapter<MutableList<Widget>>() {
    init {
        delegatesManager.addDelegate(RedWidgetAdapterDelegate())
        delegatesManager.addDelegate(OrangeWidgetAdapterDelegate())
        delegatesManager.addDelegate(YellowWidgetAdapterDelegate())
        delegatesManager.addDelegate(GreenWidgetAdapterDelegate())
        delegatesManager.addDelegate(CyanWidgetAdapterDelegate())
        delegatesManager.addDelegate(BlueWidgetAdapterDelegate())
        delegatesManager.addDelegate(PurpleWidgetAdapterDelegate())
        items = mutableListOf()
    }

    fun submitWidgets(widgets: List<Widget>) {
        val diffResult = DiffUtil.calculateDiff(WidgetsDiffCallback(items, widgets))
        diffResult.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(widgets)
    }
}