package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.app.Activity
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_green.*
import android.content.ContextWrapper



class GreenWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {
    private var item: Widget.Green? = null

    init {
        widget_btn_execute.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Common.ExecuteRunnable(it, Runnable {
                    val activityClass = getActivity(containerView)?.localClassName ?: "null"
                    Toast.makeText(containerView.context, "I am clicked while in $activityClass", Toast.LENGTH_SHORT)
                        .show()
                })
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Green) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) {
            View.VISIBLE
        } else {
            View.GONE
        }
        widget_btn_execute.visibility = if (widget.hasButton) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun getActivity(view: View): Activity? {
        var context = view.context
        while (context is ContextWrapper) {
            if (context is Activity) {
                return context
            }
            context = context.baseContext
        }
        return null
    }
}