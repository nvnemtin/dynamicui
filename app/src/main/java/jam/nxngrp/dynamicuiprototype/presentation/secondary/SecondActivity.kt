package jam.nxngrp.dynamicuiprototype.presentation.secondary

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.presentation.adapter.WidgetsAdapter
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    private lateinit var viewModel: SecondViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val adapter = WidgetsAdapter()
        widgets_rv.layoutManager = GridLayoutManager(this, 2)
        widgets_rv.adapter = adapter
        viewModel = ViewModelProviders.of(this).get(SecondViewModel::class.java)
        viewModel.viewState.observe(this, Observer {
            swipe_refresh.isRefreshing = it is SecondViewModel.ViewState.Loading
            when (it) {
                is SecondViewModel.ViewState.Loading -> {
                }
                is SecondViewModel.ViewState.Content -> {
                    adapter.submitWidgets(it.widgets)
                }
                is SecondViewModel.ViewState.Error -> {
                    Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.navSignal.observe(this, Observer {
            when (it) {
                "SECOND" -> startActivity(Intent(this, SecondActivity::class.java))
                "BACK" -> onBackPressed()
                else -> {
                }
            }
        })
        swipe_refresh.setOnRefreshListener {
            viewModel.refresh()
        }
        if (savedInstanceState == null) {
            viewModel.refresh()
        }
    }
}
