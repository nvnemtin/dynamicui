package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.handler.BlueWidgetProducer
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_blue.*

class BlueWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Widget.Blue? = null

    init {
        widget_btn_start_producer.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Common.StartProducer(it)
                it.dispatcher?.putHandler(it.id, BlueWidgetProducer())
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Blue) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) { View.VISIBLE } else { View.GONE }
        widget_btn_start_producer.visibility = if (widget.hasButton) { View.VISIBLE } else { View.GONE }
    }
}