package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.PurpleWidgetViewHolder

class PurpleWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Purple, Widget, PurpleWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): PurpleWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_purple, parent, false)
        return PurpleWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Purple
    }

    override fun onBindViewHolder(item: Widget.Purple, holder: PurpleWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}