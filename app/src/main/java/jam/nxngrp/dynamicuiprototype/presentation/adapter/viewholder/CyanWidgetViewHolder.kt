package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.WidgetStateBlockingProducer
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.handler.CyanWidgetBlockingProducer
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_cyan.*

class CyanWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Widget.Cyan? = null
    private val blockingProducer: WidgetStateBlockingProducer = CyanWidgetBlockingProducer()

    init {
        widget_btn_blocking_produce.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Common.ProduceBlocking(it)
                it.dispatcher?.putHandler(it.id, blockingProducer)
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Cyan) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) {
            View.VISIBLE
        } else {
            View.GONE
        }
        widget_btn_blocking_produce.visibility = if (widget.hasButton) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}