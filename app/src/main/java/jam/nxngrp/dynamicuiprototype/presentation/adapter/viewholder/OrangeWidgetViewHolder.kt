package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_orange.*

class OrangeWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Widget.Orange? = null

    init {
        widget_btn_update.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Common.SelfUpdate(it)
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Orange) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) { View.VISIBLE } else { View.GONE }
        widget_btn_update.visibility = if (widget.hasButton) { View.VISIBLE } else { View.GONE }
    }
}