package jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import jam.nxngrp.dynamicuiprototype.domain.command.WidgetCommand
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.widget_purple.*

class PurpleWidgetViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    private var item: Widget.Purple? = null

    init {
        widget_btn_navigate_back.setOnClickListener {
            item?.let {
                val command = WidgetCommand.Common.Navigate(it, "BACK")
                it.dispatcher?.dispatchCommand(command)
            }
        }
    }

    fun bindTo(widget: Widget.Purple) {
        this.item = widget
        widget_content.text = widget.content
        widget_progress_bar.visibility = if (widget.needRefreshing) { View.VISIBLE } else { View.GONE }
        widget_btn_navigate_back.visibility = if (widget.hasButton) { View.VISIBLE } else { View.GONE }
    }
}