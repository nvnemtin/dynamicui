package jam.nxngrp.dynamicuiprototype.presentation.main

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.presentation.adapter.WidgetsAdapter
import jam.nxngrp.dynamicuiprototype.presentation.secondary.SecondActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = WidgetsAdapter()
        widgets_rv.layoutManager = LinearLayoutManager(this)
        widgets_rv.adapter = adapter
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.viewState.observe(this, Observer {
            swipe_refresh.isRefreshing = it is MainViewModel.ViewState.Loading
            when (it) {
                is MainViewModel.ViewState.Loading -> {}
                is MainViewModel.ViewState.Content -> {
                    adapter.submitWidgets(it.widgets)
                }
                is MainViewModel.ViewState.Error -> {
                    Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.navSignal.observe(this, Observer {
            when (it) {
                "SECOND" -> startActivity(Intent(this, SecondActivity::class.java))
                "BACK" -> onBackPressed()
                else -> {}
            }
        })
        swipe_refresh.setOnRefreshListener {
            viewModel.refresh()
        }
        if (savedInstanceState == null) {
            viewModel.refresh()
        }
    }
}
