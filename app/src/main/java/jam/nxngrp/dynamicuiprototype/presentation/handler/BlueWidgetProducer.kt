package jam.nxngrp.dynamicuiprototype.presentation.handler

import io.reactivex.Observable
import jam.nxngrp.dynamicuiprototype.domain.WidgetStateProducer
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import java.util.concurrent.TimeUnit

class BlueWidgetProducer : WidgetStateProducer {
    override fun produce(widget: Widget): Observable<Widget> {
        return if (widget is Widget.Blue) {
            Observable
                .interval(2, TimeUnit.SECONDS)
                .scan(widget) { old: Widget, _ ->
                    Widget.Blue(
                        id = old.id,
                        content = old.content + ".",
                        hasButton = old.hasButton,
                        needRefreshing = old.needRefreshing,
                        dispatcher = old.dispatcher
                    ) as Widget
                }
        } else {
            Observable.just(widget)
        }
    }
}