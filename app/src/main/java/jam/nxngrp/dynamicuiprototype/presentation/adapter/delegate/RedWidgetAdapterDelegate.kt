package jam.nxngrp.dynamicuiprototype.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import jam.nxngrp.dynamicuiprototype.R
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import jam.nxngrp.dynamicuiprototype.presentation.adapter.viewholder.RedWidgetViewHolder

class RedWidgetAdapterDelegate: AbsListItemAdapterDelegate<Widget.Red, Widget, RedWidgetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): RedWidgetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.widget_red, parent, false)
        return RedWidgetViewHolder(view)
    }

    override fun isForViewType(item: Widget, items: MutableList<Widget>, position: Int): Boolean {
        return item is Widget.Red
    }

    override fun onBindViewHolder(item: Widget.Red, holder: RedWidgetViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}