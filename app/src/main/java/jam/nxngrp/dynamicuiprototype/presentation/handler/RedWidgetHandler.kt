package jam.nxngrp.dynamicuiprototype.presentation.handler

import android.content.Context
import android.util.Log
import android.widget.Toast
import jam.nxngrp.dynamicuiprototype.domain.WidgetHandler
import jam.nxngrp.dynamicuiprototype.domain.item.Widget
import java.lang.ref.WeakReference

class RedWidgetHandler(context: Context, private val injected: String) : WidgetHandler {
    private val ctx = WeakReference<Context>(context)

    fun ownLogic(widget: Widget) {
        if (widget is Widget.Red) {
            ctx.get()?.let {
                Toast.makeText(it, "Own logic with context ${ctx.get()} and $injected", Toast.LENGTH_SHORT).show()
            }
        } else {
            Log.d("ZZZ", "Red widget handler is called by someone unwanted")
        }
    }
}